package euler

import common.{all_partitions => Call_partitions}
  
object matrix {
  private def all_hor_partitions[T](xs: List[List[T]], n: Int) =
    xs.flatMap(Call_partitions(_, n))
  private def all_diag_partitions[T](xs: List[List[T]], n: Int) =
    Call_partitions(xs, n).map(_.zipWithIndex.map {
      case (p, i) =>
	p drop(i) toList}).flatMap(transpose(_))
  def all_partitions[T](xs: List[List[T]], n: Int) = all_hor_partitions(xs, n) ++ all_hor_partitions(transpose(xs), n) ++ all_diag_partitions(xs, n) ++ all_diag_partitions(xs.map(_ reverse), n)
  def transpose[T](xs: List[List[T]]) = {
    val el:List[List[T]] = xs(0).map(_ => Nil)
    xs.foldLeft(el)(
      _ zip(_) map { case (a, b) => b::a })
  }
}
