package euler

import common.resourceSource
import matrix._
  
object Problem11 {
  def apply() = {
    val s = resourceSource("problem11.txt")
    val xs = s.getLines.map(_.stripLineEnd.split(" ").map(_ toInt).toList).toList
    all_partitions(xs, 4).map(_ reduceLeft(_ * _)).reduceLeft(_ max _)
  }
}
