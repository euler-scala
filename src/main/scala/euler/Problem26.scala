package euler

import math._
  
object Problem26 {
  def isLongPeriod(d: Int) = BigInt(1 until d map(_ => "9") reduceLeft(_ + _)) % d == 0
  def apply() =
    primes takeWhile(_ < 1000) filter((x:Long) => isLongPeriod(x.toInt)) map((x:Long) => (x, prime_factors(x - 1) reduceLeft(_ max(_)))) reduceLeft{ (a, b) => if(a._2 > b._2) a else b } 
}
