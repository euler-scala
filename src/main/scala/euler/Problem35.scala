package euler

import math._
  
object Problem35 {
  def apply() = fixed_filtered_primes filter{
    x =>
    val rs = rotations(x)
    rs.isEmpty || rs.forall(prime_?)             
  } length
  private def fixed_filtered_primes:Stream[Long] = Stream.cons(2, Stream.cons(5, filtered_primes))
  private def filtered_primes = primes takeWhile(_ < 1000000) filter(_.toString().toList.map(_ toString).forall(List("1","3","7","9") contains(_)))
  private def rotations(x: Long) = {
    val s = x.toString()
    1 until s.length map(n => (s.drop(n) + s.take(n)) toLong)
  }
}
