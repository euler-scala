package euler

import math._
import common._
import triangles._

object problems {
  def problem3 = prime_factors(600851475143L)(3)
  def problem4 = {
    val r = 100 to 999
    (for{a <- r
	 b <- r
	 prod = a * b
	 if palyndrome_?(prod)}
     yield prod).reduceLeft(_.max(_))
  }
  def problem5 = 1 to 20 map(_.toLong) reduceLeft(lcm)
  def problem6:Long = {
    val r = 1 to 100
    (Math.pow(r.reduceLeft(_ + _), 2) -
    r.map(Math.pow(_, 2)).reduceLeft(_ + _)) longValue    
  }
  def problem7 = primes(10000)
  def problem8 = {
    val s = resourceSource("problem8.txt")
    val cs = s.getLines.map(_ stripLineEnd).reduceLeft(_ + _)
    val ds = cs.toList.map(_ toString() toInt)
    val seqs = all_partitions(ds, 5)
    seqs map(_ reduceLeft(_ * _)) reduceLeft(_ max(_))
  }
  def problem9 = (
    for{
      a <- 1 to 999
      b <- a to 999
      c <- b to 999
      if pythagorean_?(a, b, c) && a + b + c == 1000}
    yield a * b * c)(0)
  def problem10 = primes takeWhile(_ < 2000000) reduceLeft(_ + _)
  def problem11 = Problem11()
  def problem12 = triangle_number(propers.map(triangle_divisors_num(_)).zipWithIndex.dropWhile(_._1 <= 500).head._2 + 1)
  def problem13 = {
    val s = resourceSource("problem13.txt")
    val xs = s.getLines.map((l:String) => BigInt(l stripLineEnd))
    xs.reduceLeft(_ + _).toString().substring(0, 10)
  }
  def problem14 = {
    propers.takeWhile(_ < 1000000).map(x => (x, Problem14()(x))).
    foldLeft((0L,0L)){ (a, b) =>
      if(b._2 > a._2)
        b
      else
        a }._1
  }
  def problem15 = Problem15()(20, 20)
  def problem16 = BigInt(2).pow(1000).toString().toList.map(_ toString() toInt).reduceLeft(_ + _)
  def problem17 = 1 to 1000 map(euler.problem17.Problem17(_)) mkString " " replace(" ", "") length
  def problem26 = Problem26()
  def problem29 = Problem29()
  def problem36 = Problem36()
  def problem34 = Problem34()
  def problem35 = Problem35()
}
