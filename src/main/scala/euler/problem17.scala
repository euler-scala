package euler.problem17

import scala.collection.mutable.ListBuffer
  
abstract class Part(conts: String)

case class Hundreds(conts: String) extends Part(conts toString) {
  override def toString() = Twenties(conts).toString() + " hundred"
}
case class Ninetith(conts: String) extends Part(conts toString) {
  override def toString() = {
    (conts.take(1).toString match {
      case "0" => ""
      case "2" => "twenty"
      case "3" => "thirty"
      case "4" => "forty"
      case "5" => "fifty"
      case "6" => "sixty"
      case "7" => "seventy"
      case "8" => "eighty"
      case "9" => "ninety"
    }) +
    Twenties(conts drop(1)).toString
  }
}
case class Twenties(conts: String) extends Part(conts toString) {
  override def toString() = conts match {
    case "0" => ""
    case "1" => "one"
    case "2" => "two"
    case "3" => "three"
    case "4" => "four"
    case "5" => "five"
    case "6" => "six"
    case "7" => "seven"
    case "8" => "eight"
    case "9" => "nine"
    case "10" => "ten"
    case "11" => "eleven"
    case "12" => "twelve"
    case "13" => "thirteen"
    case "14" => "fourteen"
    case "15" => "fifteen"
    case "16" => "sixteen"
    case "17" => "seventeen"
    case "18" => "eighteen"
    case "19" => "nineteen"
  }
}

object Number {
  def apply(n: Int):String = {
    if(n == 1000)
      return "one thousand"
    val rns = n toString() reverse
    val ps:ListBuffer[Part] = new ListBuffer()
    if(rns.length > 2)
      ps += Hundreds(rns.drop(2).reverse)
    val firstTwoDigits = rns.take(2).reverse
    if(firstTwoDigits.toString != "00") {
      val tfd = if(firstTwoDigits.toInt > 19) Ninetith(firstTwoDigits)
                else Twenties(firstTwoDigits.toInt.toString)
      ps += tfd
    }
    new Number(ps toList).toString
  }
}
class Number(parts: List[Part]) {
  override def toString() = parts map(_ toString) mkString " and "
}
  
object Problem17 {
  def apply(n:Int) = Number(n)
}
