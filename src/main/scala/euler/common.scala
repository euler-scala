package euler

import scala.io.Source
  
object common {
  def palyndrome_?[T](x: T) = {
    val s = x toString()
    s.reverse.toString == s
  }
  def resourceSource(name: String) = {
    val cl = getClass.getClassLoader
    val is = cl.getResourceAsStream(name)
    Source.fromInputStream(is)
  }
  def all_partitions[T](xs:List[T], n: Int):List[List[T]] = 0 until n flatMap((x:Int) => partition(xs drop(x), n)) toList
  def partition[T](xs: List[T], n: Int):List[List[T]] = xs.splitAt(n) match {
    case (pred, Nil) => if(pred.length == n) pred::Nil
			else Nil
    case (pred, rest) => pred::partition(rest, n)
  }
  def forany[T](xs: List[T], p:(T) => Boolean) = xs find(p) match {
    case Some(_) => true
    case None => false
  }
}
