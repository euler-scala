package euler

object Problem30 {
  def toDigitsPower(x: Int, p: Int) = {
    val s = x.toString
    s.toList.map((x:Char) => Math pow(x.toString.toInt, p)).reduceLeft(_ + _)
  }
  def apply() = 2 to 1000000 filter(x => toDigitsPower(x, 5) == x) reduceLeft(_ + _)
}
