package euler

import math._
  
object triangles {  
  private def triangle_pair(n:Long) = if(even_?(n)) (n / 2, n + 1) else ((n + 1) / 2, n)
  def triangle_divisors_num(n:Long) = {
    val (a, b) = triangle_pair(n)
    val pf = Set(prime_factors(a):_*) ++ prime_factors(b)
    val tn = triangle_number(n)    
    pf.toList.map{ d => {
      var tn_ = tn / d
      var c = 1      
      while(tn_ % d == 0) {
        tn_ /= d
        c += 1
      }
      c
    }}.foldLeft(1)((a, b) => a * (b + 1))
  }
  def triangle_number(n:Long) = n * (n + 1) / 2
  def triangle_numbers = propers.map(n => triangle_number(n))
}
