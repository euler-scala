package euler

import math._
import scala.collection.mutable.Map

object Problem14 extends MetaProblem[Problem14] {
  def p = new Problem14
}
  
class Problem14 {
  private val ls: Map[Long, Long] = Map()
  def apply(n:Long):Long = {    
    def loop(n_ :Long, acc:Long):Long = {
      if(ls.contains(n_)) {
        ls += n -> (ls(n_) + acc)
        ls(n_) + acc
      }
      else n_ match {
        case 1 => {
          ls += n -> (1 + acc)
          1 + acc
        }
        case _ => loop(if(even_?(n_)) n_ / 2 else 3 * n_ + 1, 1 + acc)
      }
    }
    loop(n, 0)
  }
}
