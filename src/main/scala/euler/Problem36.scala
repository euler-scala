package euler

import common._
  
object Problem36 {
  def apply() = 1 until 1000000 filter(x => palyndrome_?(x) && palyndrome_?(x toBinaryString)) reduceLeft(_ + _)
}
