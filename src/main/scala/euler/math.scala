package euler

import scala.collection.mutable.BitSet
  
object math {
  private def propers_(n: Long):Stream[Long] = Stream.cons(n, propers_(n + 1))
  lazy val propers = propers_(1)
  lazy val primes = propers.filter(prime_?)
  def prime_?(n:Long) = BigInt(n) isProbablePrime(10)
  def factor_pred(n:Long) = (f:Long) =>  n % f == 0
  def prime_factors(n: Long) = primes takeWhile(_ <= n) filter(factor_pred(n))
  def lcm(a:Long, b:Long) = a * b / (BigInt(a).gcd(b)).longValue
  def pythagorean_?(a:Int, b:Int, c:Int) = a * a + b * b == c * c
  private def divisors(n: Long) = Stream.cons(1, Stream.cons(n, propers drop(1) takeWhile(_ < n) filter(n % _ == 0)))
  def even_?(n:Long) = n % 2 == 0
  def fac(n: Int):Long = n match {
    case 1 => 1
    case 0 => 1
    case _ => 2 to n reduceLeft(_ * _)
  }
}
