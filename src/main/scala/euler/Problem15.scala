package euler

import scala.collection.mutable.Map

object Problem15 extends MetaProblem[Problem15] {def p = new Problem15}
  
class Problem15 {
  private val vs: Map[(Int, Int), Long] = Map()
  private def exchange(x: Int, y:Int) = (x.min(y), x.max(y))
  private def vars(x:Int, y:Int):Long = {
    val (x_,y_) = exchange(x,y)
    if(vs.contains((x_,y_)))
      vs((x_,y_))
    else if(x_ == 0 && y_ == 0)
      0
    else if(x_ == 0)
      vars(x_, y_ - 1)
    else if(y == 0)
      vars(x_ - 1, y_)
    else
      1 + vars(x_ - 1, y_) + vars(x_, y_ - 1)
  }
  def apply(mx:Int, my:Int) = {
    for{
      x <- 0 to mx
      y <- 0 to my
    }
    vs += (x, y) -> vars(x, y)
    vs((mx, my)) + 1
  }
}
