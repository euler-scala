package euler

abstract class MetaProblem[P] {
  def p:P
  def apply() = p
}
