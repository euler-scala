package euler

object Problem48 {
  def apply() = (1 to 1000).map(x => BigInt(x) modPow(x, 10000000000L)).reduceLeft(_ + _).toString.reverse.take(10).reverse
}
