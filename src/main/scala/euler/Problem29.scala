package euler

import scala.collection.mutable.Set
  
object Problem29 {  
  def apply() = {
    val ts:Set[BigInt] = Set()
    for {
      a <- 2 to 100
      b <- 2 to 100
    } ts += BigInt(a).pow(b)
    ts size
  }
}
