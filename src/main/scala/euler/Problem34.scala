package euler

import math._
  
object Problem34 extends MetaProblem[Problem34] { def p = new Problem34 }

class Problem34 {
  private val facs = 0 until 10 map(fac(_))
  private def toSumOfFacs(n: Long) = n.toString.toList.map((x:Char) => facs(x.toString.toInt)).reduceLeft(_ + _)
  def apply() = 3 to 100000 filter(x => toSumOfFacs(x) == x) reduceLeft(_ + _)
}
